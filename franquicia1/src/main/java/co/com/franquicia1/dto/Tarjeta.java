package co.com.franquicia1.dto;

public class Tarjeta {
	
	private String nombreTarjeta;
	private String codigoSeguridad;
	private String vencimiento;
	private String numeroTarjeta;
	
	
	
	
	public Tarjeta() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Tarjeta(String nombreTarjeta, String codigoSeguridad, String vencimiento, String numeroTarjeta) {
		super();
		this.nombreTarjeta = nombreTarjeta;
		this.codigoSeguridad = codigoSeguridad;
		this.vencimiento = vencimiento;
		this.numeroTarjeta = numeroTarjeta;
	}
	public String getNombreTarjeta() {
		return nombreTarjeta;
	}
	public void setNombreTarjeta(String nombreTarjeta) {
		this.nombreTarjeta = nombreTarjeta;
	}
	public String getCodigoSeguridad() {
		return codigoSeguridad;
	}
	public void setCodigoSeguridad(String codigoSeguridad) {
		this.codigoSeguridad = codigoSeguridad;
	}
	public String getVencimiento() {
		return vencimiento;
	}
	public void setVencimiento(String vencimiento) {
		this.vencimiento = vencimiento;
	}
	public String getNumeroTarjeta() {
		return numeroTarjeta;
	}
	public void setNumeroTarjeta(String numeroTarjeta) {
		this.numeroTarjeta = numeroTarjeta;
	}
	
	

}
