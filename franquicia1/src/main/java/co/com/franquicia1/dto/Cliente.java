package co.com.franquicia1.dto;

public class Cliente {

	private String apellidos;
	private String nombres;
	private String id;
	private String tipoId;
	
	
	public Cliente(String apellidos, String nombres, String id, String tipoId) {
		
		this.apellidos = apellidos;
		this.nombres = nombres;
		this.id = id;
		this.tipoId = tipoId;
	}
	
	public Cliente() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTipoId() {
		return tipoId;
	}
	public void setTipoId(String tipoId) {
		this.tipoId = tipoId;
	}
	
	
}
