package co.com.franquicia1.fachada;

import co.com.franquicia1.dto.Compra;
import co.com.franquicia1.dto.DatosGeneracionTarjeta;
import co.com.franquicia1.dto.Tarjeta;

public interface IFachadaTarjeta {
	
	//Crear tarjeta
	public Tarjeta crearTarjeta(DatosGeneracionTarjeta datosGeneracionTarjeta);
	
	public boolean registrarCompra(Compra compra);
	
	public Tarjeta consultarTarjeta(String numero);

}
