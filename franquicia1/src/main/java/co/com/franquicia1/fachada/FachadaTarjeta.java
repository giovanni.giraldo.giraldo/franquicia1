package co.com.franquicia1.fachada;

import co.com.franquicia1.dto.Compra;
import co.com.franquicia1.dto.DatosGeneracionTarjeta;
import co.com.franquicia1.dto.Tarjeta;
import co.com.franquicia1.servicios.ServicioTarjetas;

public class FachadaTarjeta implements IFachadaTarjeta {

	private ServicioTarjetas servicio;
	
	@Override
	public Tarjeta crearTarjeta(DatosGeneracionTarjeta datosGeneracionTarjeta) {
		return servicio.crearTarjeta(datosGeneracionTarjeta);
	}

	@Override
	public boolean registrarCompra(Compra compra) {
		return servicio.registrarCompra(compra);
	}

	@Override
	public Tarjeta consultarTarjeta(String numero) {
		return servicio.consultarTarjeta(numero);
	}

}
