
package co.com.franquicia1;

import javax.jws.WebService;

@WebService(endpointInterface = "co.com.franquicia1.HelloWorld")
public class HelloWorldImpl implements HelloWorld {

    public String sayHi(String text) {
        return "Hello " + text;
    }
}

