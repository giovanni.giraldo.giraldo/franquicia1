package co.com.franquicia1.servicios;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Random;

import co.com.franquicia1.dto.DatosGeneracionTarjeta;
import co.com.franquicia1.dto.Tarjeta;

public class GeneradorTarjeta {
	private static final int DIGITOS_SEGURIDAD=3;
	private static final int DIGITOS_TARJETA=12;
	private static final String DIGITOS_STA_TARJETA= "6016";
	private static final int VIGENCIA = 3;
	
	Random random = new Random(); 
	public Tarjeta generar(DatosGeneracionTarjeta datosgeneracionTarjeta) {
		validarDatos(datosgeneracionTarjeta);
		Tarjeta tarjeta = new Tarjeta();
		tarjeta.setNombreTarjeta(crearNombre(datosgeneracionTarjeta));
		tarjeta.setCodigoSeguridad(crearCodigoSeguridad());
		tarjeta.setNumeroTarjeta(numeroTarjeta());
		tarjeta.setVencimiento(generarFechaVencimiento());
		return tarjeta;
	}
	
	private void validarDatos(DatosGeneracionTarjeta datosgeneracionTarjeta) {
		// TODO Auto-generated method stub
		if (datosgeneracionTarjeta.getBanco() == null) {
			throw new ExceptionCrearTarjeta("El Bancon no puede ser Nulo.");
			//datosgeneracionTarjeta.equals("BBVA")	esto me puede generar un error de null pointer exception se debe hacer de la siguiente manera
		} 
		if ("BBVA".equals(datosgeneracionTarjeta.getBanco())) {
			throw new ExceptionCrearTarjeta("No se pueden generar tarjetas de este banco.");
		}
		
		if (datosgeneracionTarjeta.getCliente() ==null) {
			throw new ExceptionCrearTarjeta("debe digitar el apellido");
		}
		if (datosgeneracionTarjeta.getCliente().getApellidos() ==null) {
			throw new ExceptionCrearTarjeta("Debe Digitar Apellidos.");
		}
		if (datosgeneracionTarjeta.getCliente().getNombres() ==null) {
			throw new ExceptionCrearTarjeta("Debe Digitar Nombre.");
		}
	}

	private String crearNombre(DatosGeneracionTarjeta datosgeneracioTarjeta) {
				
		return datosgeneracioTarjeta.getCliente().getApellidos().toUpperCase() + ' '+ datosgeneracioTarjeta.getCliente().getNombres().toUpperCase() ;
	}
	
	private String crearCodigoSeguridad () {
		StringBuilder numero = new StringBuilder();
		
		
		for (int i = 1; i <= DIGITOS_SEGURIDAD; i++) {
			int numeroRandom =random.nextInt(9);
			numero.append(Integer.toString(numeroRandom));
		}
		return numero.toString();
	}
	
	private String numeroTarjeta() {
		
		StringBuilder numero =new StringBuilder();
		for (int i = 1; i <= DIGITOS_TARJETA; i++) {
			int numerorandom = random.nextInt(9);
			numero.append(Integer.toString(numerorandom));
		}
		return DIGITOS_STA_TARJETA + numero.toString();
	}
	
	private String generarFechaVencimiento() {
		LocalDate localdatehoy = LocalDate.now();
		LocalDate localDateVencimiento = localdatehoy.plusYears(VIGENCIA);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/YY");
		return localDateVencimiento.format(formatter);
				
				
				
				
	}

}
