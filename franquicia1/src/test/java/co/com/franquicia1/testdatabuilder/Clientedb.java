package co.com.franquicia1.testdatabuilder;

import co.com.franquicia1.dto.Cliente;

public class Clientedb {
		private String apellidos;
		private String nombres;
		private String id;
		private String tipoId;
		
		public Clientedb() {
			this.apellidos = "GIRALDO";
			this.nombres = "GIOVANNI";
			this.id = "71275";
			this.tipoId = "CC";
		}
		public  Clientedb whithApellido(String apellidos) {
			this.apellidos = apellidos;
			return this;
		}
		public   Clientedb whithNombres(String nombres) {
			this.nombres = nombres;
			return this;
			
		}
		public   Clientedb whithId(String id) {
			this.id = id;
			return this;
		}
		public   Clientedb whithTipoId(String tipoId) {
			this.tipoId = tipoId;
			return this;
		}
		
		public Cliente build() {
			Cliente cliente = new Cliente(this.apellidos,this.nombres,this.id,this.tipoId);
			return cliente;
		}
	 
	

	}
