package co.com.franquicia1.servicios;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import co.com.franquicia1.dto.Cliente;
import co.com.franquicia1.dto.DatosGeneracionTarjeta;
import co.com.franquicia1.dto.Tarjeta;

public class GeneradorTarjetaTest {
	private GeneradorTarjeta generandoTarjetas;
	@Before
	public void iniciarTest() {
		generandoTarjetas = new GeneradorTarjeta();
	}
	@Test 
	public void testnombre() {
		//Arrange
		Cliente cliente = new Cliente();
		cliente.setApellidos("Giraldo");
		cliente.setNombres("Giovanni");
		cliente.setId("12345");
		cliente.setTipoId("CC");
		
		DatosGeneracionTarjeta datosGenerarTarejeta = new DatosGeneracionTarjeta();
		datosGenerarTarejeta.setBanco("Bancolombia");
		datosGenerarTarejeta.setCliente(cliente);
		datosGenerarTarejeta.setCupo(1000);
		
		//Act
		Tarjeta tarjeta = generandoTarjetas.generar(datosGenerarTarejeta);
	
		//Assert
		assertEquals("GIRALDO GIOVANNI",tarjeta.getNombreTarjeta());
		
	}
	@Test 
	public void testseguridad() {
		//Arrange
		Cliente cliente = new Cliente();
		cliente.setApellidos("Giraldo");
		cliente.setNombres("Giovanni");
		cliente.setId("12345");
		cliente.setTipoId("CC");
		
		DatosGeneracionTarjeta datosGenerarTarejeta = new DatosGeneracionTarjeta();
		datosGenerarTarejeta.setBanco("Bancolombia");
		datosGenerarTarejeta.setCliente(cliente);
		datosGenerarTarejeta.setCupo(1000);
		
		//Act
		Tarjeta tarjeta = generandoTarjetas.generar(datosGenerarTarejeta);
	
		//Assert
	
		assertEquals(3, tarjeta.getCodigoSeguridad().length());
	}
	@Test 
	public void testnumeroTarjeta() {
		//Arrange
		Cliente cliente = new Cliente();
		cliente.setApellidos("Giraldo");
		cliente.setNombres("Giovanni");
		cliente.setId("12345");
		cliente.setTipoId("CC");
		
		DatosGeneracionTarjeta datosGenerarTarejeta = new DatosGeneracionTarjeta();
		datosGenerarTarejeta.setBanco("Bancolombia");
		datosGenerarTarejeta.setCliente(cliente);
		datosGenerarTarejeta.setCupo(1000);
		
		//Act
		Tarjeta tarjeta = generandoTarjetas.generar(datosGenerarTarejeta);
	
		//Assert
	
		assertEquals(16, tarjeta.getNumeroTarjeta().length());
	}
	@Test 
	public void testgenerarFechaVencimiento() {
		//Arrange
		Cliente cliente = new Cliente();
		cliente.setApellidos("Giraldo");
		cliente.setNombres("Giovanni");
		cliente.setId("12345");
		cliente.setTipoId("CC");
		
		DatosGeneracionTarjeta datosGenerarTarejeta = new DatosGeneracionTarjeta();
		datosGenerarTarejeta.setBanco("Bancolombia");
		datosGenerarTarejeta.setCliente(cliente);
		datosGenerarTarejeta.setCupo(1000);
		
		//Act
		Tarjeta tarjeta = generandoTarjetas.generar(datosGenerarTarejeta);
	
		//Assert
	
		assertEquals(5, tarjeta.getVencimiento().length());
	}
	@Test 
	public void testgenerarNombre() {
		//Arrange
		Cliente cliente = new Cliente();
		cliente.setApellidos(null);
		cliente.setNombres("Giovanni");
		cliente.setId("12345");
		cliente.setTipoId("CC");
		
		DatosGeneracionTarjeta datosGenerarTarejeta = new DatosGeneracionTarjeta();
		datosGenerarTarejeta.setBanco("Bancolombia");
		datosGenerarTarejeta.setCliente(cliente);
		datosGenerarTarejeta.setCupo(1000);
		
		//Act
		Tarjeta tarjeta = generandoTarjetas.generar(datosGenerarTarejeta);
	
		//Assert
	
		assertEquals(5, tarjeta.getVencimiento().length());
	}
	@Test 
	public void testprueb() {
		assertTrue(true);
	}
}
